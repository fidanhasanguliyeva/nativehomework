import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";

import { Field } from "./Field";
import { CustomText } from "./CustomText";

export const CountField = ({ width, onChangeText, value, ...rest }) => {
  const changeHandler = (value) => {
    if (+value < 0 || isNaN(value)) return;
    onChangeText(value);
  };

  const increaseCount = () => changeHandler(+value + 1);
  const decreaseCount = () => changeHandler(+value - 1);
  return (
    <View style={{ width }}>
      <TouchableOpacity
        style={[styles.btn, styles.minusBtn]}
        onPress={decreaseCount}
      >
        <CustomText weight="bold" style={styles.btnText}>
          -
        </CustomText>
      </TouchableOpacity>
      <Field
        {...rest}
        value={value.toString()}
        keyboardType="number-pad"
        onChangeText={changeHandler}
      />
      <TouchableOpacity
        style={[styles.btn, styles.plusBtn]}
        onPress={increaseCount}
      >
        <CustomText weight="bold" style={styles.btnText}>
          +
        </CustomText>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  btn: {
    position: "absolute",
    height: 42,
    width: 30,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
    zIndex: 2,
  },
  minusBtn: {
    left: 0,
  },
  plusBtn: {
    right: 0,
  },
});

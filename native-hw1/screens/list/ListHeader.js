import React from "react";
import { View, StyleSheet } from "react-native";
import { CustomBtn, CustomText } from "../../components";
import { GLOBAL_STYLES } from "../../styles/globalStyles";

export const ListHeader = ({ type, list, resetHandler }) => {
  const completed = list.map(
    (item) => item.products.filter((item) => item.completed == true).length
  );

  const allItems = list.map((item) => item.products.length);

  return (
    <View style={styles.row}>
      <View>
        {type === "Regular" && (
          <CustomBtn
            title="reset"
            style={styles.resetBtn}
            onPress={() => {
              resetHandler();
            }}
          />
        )}
      </View>
      <CustomText style={styles.progress}>
        {completed}/{allItems}
      </CustomText>
    </View>
  );
};
const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: GLOBAL_STYLES.PADDING,
  },
  resetBtn: {
    height: 19,
    paddingVertical: 3,
    paddingHorizontal: 17,
  },
  progress: {
    fontSize: 14,
  },
});

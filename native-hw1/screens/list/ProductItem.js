import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";

import { COLORS } from "../../styles/colors";
import { ICONS } from "../../styles/icons";
import { CustomText } from "../../components";

export class ProductItem extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      name,
      count,
      completed,
      onLongPress,
      onPressEdit,
      onPressDelete,
      countType,
      editMode = false,
      isOnEdit = false,
    } = this.props;
    return (
      <TouchableOpacity style={styles.container} onLongPress={onLongPress}>
        <View
          style={[
            styles.wrapper,
            {
              paddingHorizontal: editMode ? 55 : 20,
              opacity: completed && !editMode ? 0.5 : 1,
            },
          ]}
        >
          <CustomText style={styles.name}>{name}</CustomText>
          <CustomText style={styles.count}>
            x{count} {countType}
          </CustomText>
          {editMode && (
            <>
              <TouchableOpacity
                style={styles.btnEditWrapper}
                disabled={isOnEdit}
                onPress={onPressEdit}
              >
                <View
                  style={[
                    styles.btn,
                    styles.btnEdit,
                    { opacity: isOnEdit ? 0.5 : 1 },
                  ]}
                >
                  <Image source={ICONS.edit} style={styles.btnImg} />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={[styles.btn, styles.btnDelete]}
                onPress={onPressDelete}
              >
                <Image source={ICONS.remove} style={styles.btnImg} />
              </TouchableOpacity>
            </>
          )}
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    marginBottom: 14,
  },
  wrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 40,
    borderWidth: 2,
    borderColor: COLORS.SECONDARY,
    borderRadius: 20,
  },
  btn: {
    width: 40,
    height: 40,
    borderRadius: 20,
    color: "red",
    position: "absolute",
    padding: 10,
  },
  btnImg: {
    width: "100%",
    height: "100%",
  },
  btnEditWrapper: {
    position: "absolute",
    top: 0,
    left: 0,
    width: 40,
    height: 40,
  },
  btnEdit: {
    backgroundColor: COLORS.SECONDARY,
    left: -2,
    top: -2,
  },
  btnDelete: {
    backgroundColor: COLORS.PRIMARY,
    right: -2,
    top: -2,
  },
});

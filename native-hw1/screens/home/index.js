import React from "react";
import { connect } from "react-redux";
import { Alert, View, StyleSheet } from "react-native";

import { Container } from "../../commons";
import { deleteList, getData } from "../../redux/data";
import { SingleList } from "./SingleList";
import { GLOBAL_STYLES } from "../../styles/globalStyles";

const mapStateToProps = (state) => ({
  users: getData(state),
});
export const HomeScreen = connect(mapStateToProps, { deleteList })(
  ({ users, route, deleteList, navigation }) => {
    const listType = route?.params?.type || "One Time"
   
    const handleNavigation = (item) => {
      navigation.navigate("List", {
        listID: item.id,
        title: item.name,
        editMode: false,
        type: item.type,
      });
    };
    const handleDelete = (id) => {
      Alert.alert("Do you want to delete this list?", "Are you sure?", [
        {
          text: "Cancel",
          onPress: console.log("cancel pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => deleteList(id),
        },
      ]);
    };

    return (
      <Container>
        <View style={styles.container}>
          {users.map((item,i) => (
            <SingleList
              key={i}
              lists={item.lists}         
              onLongPress={handleDelete}
              listType={listType}
              onPress={handleNavigation}
            />
          ))}
        </View>
      </Container>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: GLOBAL_STYLES.PADDING
  },
});

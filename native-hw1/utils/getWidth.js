import { Dimensions } from "react-native";

export const getWidth = (percents = 100, spacesCount = 0) => {
  return ((Dimensions.get("window").width - 16 * spacesCount) / 100) * percents;
};

import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { CreateList } from "../screens";
import { headerStyle } from "../styles/headerStyle";


const { Navigator, Screen } = createStackNavigator();

export const CreateStack = () => {
  return (
    <Navigator screenOptions={headerStyle}>
      <Screen
        name="CreateList"
        component={CreateList}
        options={{ title: "New List" }}
      />
    </Navigator>
  );
};

import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { UserSettings } from "../screens";
import { headerStyle } from "../styles/headerStyle";

const { Navigator, Screen } = createStackNavigator();

export const UserSettingsStack = () => {
  return (
    <Navigator screenOptions={headerStyle}>
      <Screen
        name="UserSettings"
        component={UserSettings}
        options={{ title: "User Settings" }}
      />
    </Navigator>
  );
};
